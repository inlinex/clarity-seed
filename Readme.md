# Clarity Seed Project

This is a clearly project that their function is to start a new project
_don't_ make changes on it is only to upgrade angular or clarity system

## This Project is based on:

-   [Angular 10.1.6 CLI](https://v7.angular.io/guide/quickstart)

-   [Clarity Desing System 4.0.4](https://v4.clarity.design/get-started), Road Map of this system. [Link](https://clarity.design/news/4.0.4)

-   [HTML Codes Table](https://ascii.cl/htmlcodes.htm)

On this project are included some ignored extentions for more facilty.

# Only on the first time

Run the command "npm install"

~Provided By SmartGuy
