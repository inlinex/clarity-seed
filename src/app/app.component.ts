import { Component, VERSION } from '@angular/core';
import {} from '@clr/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'Clarity System Design';
    year = new Date().getFullYear();
    angularVer = VERSION.full;
    clrVer = '4.0.4';
}
